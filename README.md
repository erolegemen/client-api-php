# LiveGames Client Api for PHP

## +Version
0.1.0

## +Başlangıç
> TODO

> Tüm değişiklikler bu repository üzerinden yapılacaktır.  
> Lütfen güncellemeleri takip ediniz.

## +Kullanımı
Composer ile gerekli alt paketlerin yüklenmesi gerekebilir.


```php
<?php
require_once("Api.php");
$user = [
	'id' => "abc1234",  //zorunlu
	'parent' => 'ResellerAccountUserId', //zorunlu
	'name' => "Name", //zorunlu (görünecek isim)
	'surname' => "Surname", //Opsiyonel
	'phone' => "05555555555", //Opsiyonel
	'email' => "user@mail.com", //Opsiyonel - LoginBased sistemler için
	'password' => "123456" //Opsiyonel - LoginBased sistemler için
];
$api = new \LiveGames\ClientApi\Api($user, "CLIENT_KEY", "API_KEY", "API_SECRET");
?>
```

## +Api Methodları
> TODO

```php
<?php
//Token:
echo $api->token;

//Kullanıcı Kontrolü
print_r($api->checkUser());

//Yeni Kullanıcı Yaratma
print_r($api->createUser());

//Cüzdan Bilgisi
print_r($api->getWallet());

//Para Yatırma
print_r($api->deposit(100));

//Para Çekme
print_r($api->withdraw(100));


//Odanın Jackpot Bilgisi
print_r($api->jackpot('CLIENT_ROOM_ID'));


//Istatistikler

//Son Kazananlar
print_r($api->lastWinners());


//En Çok Kazananlar
print_r($api->mostWinners());


//En Çok Kazanan Numaralar
print_r($api->mostWinnerNumbers());


//En Çok Çıkan Numaralar
print_r($api->mostDrawnNumbers());


//En Çok Kazanan Kartlar:
print_r($api->mostWinnerCards());
?>
```


License
----

MIT


**i can fly!**
