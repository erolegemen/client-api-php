<?php

require_once("../src/Api.php");

$userModel = [
	'remotePk' => $_POST && @$_POST['remotePk'] ? $_POST['remotePk'] : '',  //zorunlu - sitenizdeki userId'si
	'parent' => $_POST && @$_POST['parent'] ? $_POST['parent'] : '', //zorunlu
	'name' => $_POST && @$_POST['name'] ? $_POST['name'] : '', //zorunlu (görünecek isim)
	'surname' => $_POST && @$_POST['surname'] ? $_POST['surname'] : '', //Opsiyonel
	'phone' => $_POST && @$_POST['phone'] ? $_POST['phone'] : '', //Opsiyonel
	'email' => $_POST && @$_POST['email'] ? $_POST['email'] : '', //Opsiyonel - LoginBased sistemler için
	'password' => $_POST && @$_POST['password'] ? $_POST['password'] : '' //Opsiyonel - LoginBased sistemler için
];
$api = new \LiveGames\ClientApi\Api($userModel,
	$_POST && @$_POST['API_KEY'] ? $_POST['API_KEY'] : 'API_KEY',
	$_POST && @$_POST['API_SECRET'] ? $_POST['API_SECRET'] : 'API_SECRET');

$action = $_POST && @$_POST["action"] ? $_POST["action"] : "token";
?>
<!DOCTYPE html>
<html lang="tr">
<head>
	<meta charset="utf-8">
	<title>LiveGames Client Api Test</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="LiveGames Client Api Test">
	<meta name="author" content="Livegames.io">

	<!-- Le styles -->
	<link href="assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<link href="assets/bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet">
	<style type="text/css">
		body {
			padding-top: 30px;
		}

		button {
			margin: 5px;
		}

		#outputpre {
			font-family: monospace;
			font-size: 15px;
			min-height: 350px;

			padding: 20px 5px;
			-ms-word-break: break-all;
			word-break: break-all;
			word-break: break-word;

			-webkit-hyphens: auto;
			-moz-hyphens: auto;
			hyphens: auto;

			display: block;

			margin: 0 0 10px;
			line-height: 1.42857143;
			color: #333;
			word-break: break-all;
			word-wrap: break-word;
			background-color: #f5f5f5;
			border: 1px solid #ccc;
			border-radius: 4px;
		}

		#headerpre {
			min-height: 100px;
			font-size: 85%;
		}
	</style>
</head>
<body>


<div class="container">


	<div class="row show-grid">
		<h1>LiveGames Client Test Page</h1>
		<div class="alert alert-info cold-md-12">
			<a class="close" data-dismiss="alert">&times;</a> <strong>Merhaba!</strong>
			Aşağıdaki formu kullanarak api testini yapabilirsiniz.
		</div>

		<div id="leftcolumn" class="col-md-6">
			<form id="paramform" class="well" method="post">
				<fieldset>
					<div class="row show-grid">
						<div class="row">
							<div class="col-md-4">
								<div class="control-group">
									<label class="control-label" for="API_KEY">API_KEY (Zorunlu)</label>
									<div class="controls">
										<input type="text" class="input-xlarge" id="API_KEY" name="API_KEY"
										       value="<?= $_POST && $_POST['API_KEY'] ? $_POST['API_KEY'] : "" ?>">
									</div>
								</div>
							</div>

							<div class="col-md-4">
								<div class="control-group">
									<label class="control-label" for="API_SECRET">API_SECRET (Zorunlu)</label>
									<div class="controls">
										<input type="text" class="input-xlarge" id="API_SECRET" name="API_SECRET"
										       value="<?= $_POST && $_POST['API_SECRET'] ? $_POST['API_SECRET'] : "" ?>">
									</div>
								</div>
							</div>
						</div>
					</div>
				</fieldset>
				<hr class="panel-primary">
				<fieldset>
					<div class="row show-grid">
						<div class="row">
							<div class="col-md-4">
								<div class="control-group">
									<label class="control-label" for="remotePk">UserID (Zorunlu)</label>
									<div class="controls">
										<input type="text" class="input-xlarge" id="remotePk" name="remotePk"
										       value="<?= $_POST && $_POST['remotePk'] ? $_POST['remotePk'] : "" ?>">
									</div>
								</div>
							</div>

							<div class="col-md-4">
								<div class="control-group">
									<label class="control-label" for="parent">Parent (Zorunlu)</label>
									<div class="controls">
										<input type="text" class="input-xlarge" id="parent" name="parent"
										       value="<?= $_POST && $_POST['parent'] ? $_POST['parent'] : "" ?>">
									</div>
								</div>
							</div>

							<div class="col-md-4">
								<div class="control-group">
									<label class="control-label" for="name">Ad (Zorunlu)</label>
									<div class="controls">
										<input type="text" class="input-xlarge" id="name" name="name"
										       value="<?= $_POST && $_POST['name'] ? $_POST['name'] : "" ?>">
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-4">
								<div class="control-group">
									<label class="control-label" for="surname">Soyad (opsiyonel)</label>
									<div class="controls">
										<input type="text" class="input-xlarge" id="surname" name="surname"
										       value="<?= $_POST && $_POST['surname'] ? $_POST['surname'] : "" ?>">
									</div>
								</div>
							</div>

							<div class="col-md-5">
								<div class="control-group">
									<label class="control-label" for="phone">Tel (opsiyonel)</label>
									<div class="controls">
										<input type="text" class="input-xlarge" id="phone" name="phone"
										       value="<?= $_POST && $_POST['phone'] ? $_POST['phone'] : "" ?>">
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<div class="control-group">
									<label class="control-label" for="email">Email</label>
									<div class="controls">
										<input type="text" class="input-xlarge" id="email" name="email"
										       value="<?= $_POST && $_POST['email'] ? $_POST['email'] : "" ?>">
									</div>
								</div>
							</div>

							<div class="col-md-5">
								<div class="control-group">
									<label class="control-label" for="password">Şifre (Opsiyonel-LoginBased)</label>
									<div class="controls">
										<input type="text" class="input-xlarge" id="password" name="password"
										       value="<?= $_POST && $_POST['password'] ? $_POST['password'] : "" ?>">
									</div>
								</div>
							</div>
						</div>
					</div>

					<?php if (!$api->token): ?>
						<div align="left">
							<button id="addauthbutton" class="btn btn-primary" style="margin-bottom: 1.5em;">
								<i class="icon-lock icon-white"></i> Token Oluştur
							</button>
						</div>
					<?php endif; ?>

					<?php if ($api->token): ?>
						<p class="help-block">Yaratılan Token ile işlem yapılacaktır.</p>

						<div align="left">
							<button name="action" value="checkUser" class="btn btn-info">
								<i class="icon-plus icon-white"></i> Kullanıcı Kontrolü
							</button>
							<button name="action" value="getWallet" class="btn btn-info">
								<i class="icon-plus icon-white"></i> Cüzdan Bilgisi
							</button>
							<button name="action" value="createUser" class="btn btn-success">
								<i class="icon-plus icon-white"></i> Yeni Kullanıcı
							</button>
						</div>
						<br/>

						<div align="left" class="row">
							<div class="col-md-12">
								<div class="control-group">
									<label class="control-label" for="amount">Miktar</label>
									<div class="controls">
										<input type="text" class="input-xlarge" id="amount" name="amount"
										       value="<?= $_POST && @$_POST['amount'] ? $_POST['amount'] : "" ?>">
									</div>
								</div>
							</div>
							<button class="btn btn-success" name="action" value="deposit">
								<i class="icon-plus icon-white"></i> Para Yatır
							</button>
							<button class="btn btn-danger" name="action" value="withdraw">
								<i class="icon-file icon-white"></i> Para Çek
							</button>
						</div>
						<br/><br/>


						<div align="left" class="row">
							<div class="col-md-12">
								<div class="control-group">
									<label class="control-label" for="roomId">RoomId</label>
									<div class="controls">
										<input type="text" class="input-xlarge" id="roomId" name="roomId"
										       value="<?= $_POST && @$_POST['roomId'] ? $_POST['roomId'] : "" ?>">
									</div>
								</div>
							</div>
							<button class="btn btn-primary" name="action" value="jackpot">
								<i class="icon-plus icon-white"></i> Jackpot Durumu
							</button>
						</div>
						<br/>

						<button class="btn btn-warning" name="action" value="transactions">
							<i class="icon-plus icon-white"></i> transactions
						</button>
						<br>
						<button class="btn btn-warning" name="action" value="logs">
							<i class="icon-plus icon-white"></i> logs
						</button>
						<br>

						<br>
						<div align="left" class="row">
							<button class="btn btn-warning" name="action" value="lastWinners">
								<i class="icon-plus icon-white"></i> Son Kazananlar
							</button>
							<br>
							<button class="btn btn-warning" name="action" value="mostWinners">
								<i class="icon-plus icon-white"></i> En Çok Kazananlar
							</button>
							<br>
							<button class="btn btn-warning" name="action" value="mostWinnerNumbers">
								<i class="icon-plus icon-white"></i> En Çok Kazanan Numaralar
							</button>
							<br>
							<button class="btn btn-warning" name="action" value="mostDrawnNumbers">
								<i class="icon-plus icon-white"></i> En Çok Çıkan Numaralar
							</button>
							<br>
							<button class="btn btn-warning" name="action" value="mostWinnerCards">
								<i class="icon-plus icon-white"></i> En Çok Kazanan Kartlar
							</button>
						</div>
						<br/>

					<?php endif; ?>
				</fieldset>
			</form>

			<div id="errordiv"></div>

		</div>

		<div class="col-md-6">
			<div id="ajaxoutput">
				<div class="prettyprint" id="outputpre">
					<?php
					//------ TEST METHODLARI
					switch ($action) {
						case "checkUser":
							echo "<h3>Kullanıcı Kontrolü (checkUser):</h3>";
							print_r($api->checkUser());
							break;
						case "createUser":
							echo "<h3>Kullanıcı Yaratma (createUser):</h3>";
							print_r($api->createUser());

							break;
						case "getWallet":
							echo "<h3>Cüzdan Bilgisi (getWallet):</h3>";
							print_r($api->getWallet());

							break;
						case "deposit":

							echo "<h3>Para Yatırma (deposit):</h3>";
							if ($_POST && $_POST['amount'] && $_POST['amount'] > 0) {
								print_r($api->deposit($_POST['amount']));
							} else {
								echo "MİKTAR ZORUNLUDUR!";
							}


							break;
						case "withdraw":

							echo "<h3>Para Çekme (withdraw):</h3>";
							if ($_POST && $_POST['amount'] && $_POST['amount'] > 0) {
								print_r($api->withdraw($_POST['amount']));
							} else {
								echo "MİKTAR ZORUNLUDUR!";
							}

							break;
						case "jackpot":

							echo "<h3>Jackpot:</h3>";
							if ($_POST && $_POST['roomId']) {
								print_r($api->jackpot($_POST['roomId']));
							} else {
								echo "ROOMID ZORUNLUDUR!";
							}


							break;
						case "lastWinners":
							echo "<h3>Son Kazananlar (lastWinners):</h3>";
							print_r($api->lastWinners());

							break;
						case "mostWinners":
							echo "<h3>En Çok Kazananlar (mostWinners):</h3>";
							print_r($api->mostWinners());

							break;
						case "mostWinnerNumbers":
							echo "<h3>En Çok Kazanan Numaralar (mostWinnerNumbers):</h3>";
							print_r($api->mostWinnerNumbers());

							break;

						case "mostDrawnNumbers":
							echo "<h3>En Çok Çıkan Numaralar (mostDrawnNumbers):</h3>";
							print_r($api->mostDrawnNumbers());

							break;

						case "mostWinnerCards":

							echo "<h3>En Çok Kazanan Kartlar (mostWinnerCards):</h3>";
							print_r($api->mostWinnerCards());

							break;

						case "transactions":
							echo "<h3>Bakiye İşlemleri (transactions):</h3>";
							print_r($api->transactions());

							break;
						case "logs":
							echo "<h3>İşlemler (logs):</h3>";
							print_r($api->logs());

							break;
						default:

							echo "<h3>Token:</h3>";
							echo $api->token;
							echo "<br><br>";
							print_r($api->lastError);

							break;
					}


					?>

				</div>
				<pre class="pre-scrollable prettyprint linenums"
				     id="headerpre"><?= print_r($api->lastResponse); ?></pre>

				<pre class="pre-scrollable prettyprint linenums"
				     id="headerpre"><?= print_r($api->token); ?></pre>
			</div>
		</div>
	</div>
</div>
</body>
</html>






