<?php
/*
 * Client tarafına kullanılanımı aşağıaki gibidir.
 */
require_once("../src/Handler.php");

//TODO: POST yerine hardcoded yazılması gerekmektedir.
$apiKey = $_POST && @$_POST['API_KEY'] ? $_POST['API_KEY'] : 'YOUR_KEY'; //TODO!
$apiSecret = $_POST && @$_POST['API_SECRET'] ? $_POST['API_SECRET'] : 'YOUR_SECRET'; //TODO!

//Gelen aksiyonu kendi kodunuzda kullanmak için $_POST["lgAction"] kullanılabilir.
$action = $_POST && @$_POST["lgAction"] ? $_POST["lgAction"] : "Ping"; // mvc yapısınaki controller altındaki action'dır.

//Handler
$handOfKing = new \LiveGames\ClientApi\Handler($apiKey, $apiSecret);

switch ($action) {
	case "GetWallet":

		//-- Vars ----->
		if ($handOfKing->isValidRequest) {
			$usrId = $handOfKing->request->user->id;

			echo $handOfKing->setPayload([
				'uid' => $usrId,
				'credit' => 100
			])->getResponse();
		} else {//none
			echo $handOfKing->getResponse();
		}
		break;
	case "UpdateWallet":

		//-- Vars ----->
		if ($handOfKing->isValidRequest) {
			$transactionId = $handOfKing->request->transaction->id;
			$usrId = $handOfKing->request->transaction->uid;
			$sessionId = $handOfKing->request->transaction->sid;
			$action = $handOfKing->request->transaction->action;
			$type = $handOfKing->request->transaction->type;
			$amount = $handOfKing->request->transaction->amount;

			//--your logic here
			echo $handOfKing->setPayload([
				'id' => substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10), //ResponseId
				'uid' => $usrId,
				'credit' => 222 //User Last Wallet
			])->getResponse();

		} else { //none
			echo $handOfKing->getResponse();
		}

		break;

	case "Ping":

		echo $handOfKing->setPayload([
			'pong' => true
		])->getResponse();

		break;
}