<?php
/**
 * Client-Api-Gate Test
 *
 *
 * Seamless api için..
 */

require_once("../src/Handler.php");

if (!class_exists('\Firebase\JWT\JWT')) {
	require_once("../src/lib/jwt/BeforeValidException.php");
	require_once("../src/lib/jwt/ExpiredException.php");
	require_once("../src/lib/jwt/SignatureInvalidException.php");
	require_once("../src/lib/jwt/JWT.php");
}

use \Firebase\JWT\JWT;

$API_KEY = $_POST && $_POST['API_KEY'] ? $_POST['API_KEY'] : 'API_KEY';
$API_SECRET = $_POST && $_POST['API_SECRET'] ? $_POST['API_SECRET'] : 'API_SECRET';

$doAction = $_POST && @$_POST["doAction"] ? $_POST["doAction"] : "Ping";


$tm = time();
$req = [
	"apiKey" => $API_KEY,
	"game" => "tombala",
	//"aud" => "CLIENT_HOST", (optional)
	//"nbf" => time(),
	"iat" => $tm,
	"exp" => $tm + 6400,
	"jti" => hash('crc32', $API_KEY . $tm)
];

$data = '';

switch ($doAction) {
	case 'GetWallet':
		$uid = $_POST && @$_POST["wuid"] ? $_POST["wuid"] : false;
		if ($uid) {
			$req['user'] = ['id' => $uid];
		}
		break;
	case 'UpdateWallet':
		$transactionId = $_POST && @$_POST["id"] ? $_POST["id"] : false;
		$usrId = $_POST && @$_POST["uid"] ? $_POST["uid"] : false;
		$sessionId = $_POST && @$_POST["sid"] ? $_POST["sid"] : false;
		$action = $_POST && @$_POST["action"] ? $_POST["action"] : false;
		$type = $_POST && @$_POST["type"] ? $_POST["type"] : false;
		$amount = $_POST && @$_POST["amount"] ? $_POST["amount"] : false;

		if ($transactionId && $usrId && $sessionId && $action && $type && $amount) {
			$req['transaction'] = [
				'id' => $transactionId,
				'uid' => $usrId,
				'sid' => $sessionId,
				'action' => $action,
				'type' => $type,
				'amount' => $amount
			];
		}
		break;

	case 'Ping':
	default:
		$req['ping'] = true;
		break;
}
$data = JWT::encode($req, $API_SECRET);

?>
<!DOCTYPE html>
<html lang="tr">
<head>
	<meta charset="utf-8">
	<title>LiveGames Client Api Test</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="LiveGames Client Api Test">
	<meta name="author" content="Livegames.io">

	<!-- Le styles -->
	<link href="assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<link href="assets/bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet">
	<style type="text/css">
		body {
			padding-top: 30px;
		}

		button {
			margin: 5px;
		}

		#outputpre {
			font-family: monospace;
			font-size: 15px;
			min-height: 350px;

			padding: 20px 5px;
			-ms-word-break: break-all;
			word-break: break-all;
			word-break: break-word;

			-webkit-hyphens: auto;
			-moz-hyphens: auto;
			hyphens: auto;

			display: block;

			margin: 0 0 10px;
			line-height: 1.42857143;
			color: #333;
			word-break: break-all;
			word-wrap: break-word;
			background-color: #f5f5f5;
			border: 1px solid #ccc;
			border-radius: 4px;
		}

		#headerpre {
			min-height: 100px;
			font-size: 85%;
		}
	</style>
</head>
<body>


<div class="container">


	<div class="row show-grid">
		<h1>LiveGames Client Test Page</h1>
		<div class="alert alert-info cold-md-12">
			<a class="close" data-dismiss="alert">&times;</a> <strong>Merhaba!</strong>
			Aşağıdaki formu kullanarak api-handler testini yapabilirsiniz.
		</div>

		<div id="leftcolumn" class="col-md-6">
			<form id="paramform" class="well" method="post">
				<fieldset>
					<div class="row show-grid">
						<div class="row">
							<div class="col-md-4">
								<div class="control-group">
									<label class="control-label" for="API_KEY">API_KEY (Zorunlu)</label>
									<div class="controls">
										<input type="text" class="input-xlarge" id="API_KEY" name="API_KEY"
										       value="<?= $_POST && $_POST['API_KEY'] ? $_POST['API_KEY'] : "" ?>">
									</div>
								</div>
							</div>

							<div class="col-md-4">
								<div class="control-group">
									<label class="control-label" for="API_SECRET">API_SECRET (Zorunlu)</label>
									<div class="controls">
										<input type="text" class="input-xlarge" id="API_SECRET" name="API_SECRET"
										       value="<?= $_POST && $_POST['API_SECRET'] ? $_POST['API_SECRET'] : "" ?>">
									</div>
								</div>
							</div>
						</div>
					</div>
				</fieldset>
				<hr class="panel-primary">
				<fieldset>

					<div align="left">
						<button name="doAction" value="Ping" class="btn btn-info">
							<i class="icon-plus icon-white"></i> Ping
						</button>
					</div>
					<br/>

					<div align="left" class="row">
						<div class="col-md-12">
							<div class="control-group">
								<label class="control-label" for="wuid">$usrId</label>
								<div class="controls">
									<input type="text" class="input-xlarge" id="wuid" name="wuid"
									       value="<?= $_POST && @$_POST['wuid'] ? $_POST['wuid'] : "" ?>">
								</div>
							</div>
						</div>
						<button name="doAction" value="GetWallet" class="btn btn-info">
							<i class="icon-plus icon-white"></i> Cüzdan Bilgisi
						</button>
					</div>
					<br>

					<div align="left" class="row">
						<div class="col-md-12">
							<div class="control-group">
								<label class="control-label" for="id">$transactionId</label>
								<div class="controls">
									<input type="text" class="input-xlarge" id="id" name="id"
									       value="<?= $_POST && @$_POST['id'] ? $_POST['id'] : "" ?>">
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="control-group">
								<label class="control-label" for="uid">uid</label>
								<div class="controls">
									<input type="text" class="input-xlarge" id="uid" name="uid"
									       value="<?= $_POST && @$_POST['uid'] ? $_POST['uid'] : "" ?>">
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="control-group">
								<label class="control-label" for="sid">sid</label>
								<div class="controls">
									<input type="text" class="input-xlarge" id="sid" name="sid"
									       value="<?= $_POST && @$_POST['sid'] ? $_POST['sid'] : "" ?>">
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="control-group">
								<label class="control-label" for="type">type</label>
								<div class="controls">
									<input type="text" class="input-xlarge" id="type" name="type"
									       value="<?= $_POST && @$_POST['type'] ? $_POST['type'] : "" ?>">
									* buy, deposit, withdraw, penalty, reward
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="control-group">
								<label class="control-label" for="action">action</label>
								<div class="controls">
									<input type="text" class="input-xlarge" id="action" name="action"
									       value="<?= $_POST && @$_POST['action'] ? $_POST['action'] : "" ?>">
									* Card, Jackpot, Reward, TossUp, deposit, withdraw
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="control-group">
								<label class="control-label" for="amount">Miktar</label>
								<div class="controls">
									<input type="text" class="input-xlarge" id="amount" name="amount"
									       value="<?= $_POST && @$_POST['amount'] ? $_POST['amount'] : "" ?>">
								</div>
							</div>
						</div>

						<button class="btn btn-success" name="doAction" value="UpdateWallet">
							<i class="icon-plus icon-white"></i> Cüzdan Güncelleme İşlemi
						</button>
					</div>
					<br/><br/>


				</fieldset>
			</form>

			<form id="rform" class="well" method="post" action="apigate.php" target="_blank">
				<h4><?= $doAction ?></h4>
				<input type="hidden" name="lgAction"
				       value="<?= $_POST && @$_POST['doAction'] ? $_POST['doAction'] : "" ?>">
				<input type="hidden" name="lgData" value="<?= $data ?>">

				<input type="text" class="input-xlarge" id="API_KEY" name="API_KEY"
				       value="<?= $_POST && $_POST['API_KEY'] ? $_POST['API_KEY'] : "" ?>">

				<input type="text" class="input-xlarge" id="API_SECRET" name="API_SECRET"
				       value="<?= $_POST && $_POST['API_SECRET'] ? $_POST['API_SECRET'] : "" ?>">

				<pre><?= $data ?></pre>
				<button class="btn btn-success" name="doIT">
					<i class="icon-plus icon-white"></i> Sorgula ->
				</button>
			</form>

		</div>
	</div>
</div>
</body>
</html>






